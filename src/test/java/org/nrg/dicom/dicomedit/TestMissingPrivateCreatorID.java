package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.*;

public class TestMissingPrivateCreatorID {

    @Test
    public void testMissingCreatorID() {

        String script =
                "(0010,0010) := \"pn\" \n"
                        + "(0010,0020) := (0021,{PT Test Creator ID}10)";

        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        // normal private tag
        int[] p_creator_id = {0x00210010};
        int[] p1 = {0x00211010};
        int t1 = 0x00100020;

//        src_dobj.putString( p_creator_id, "PT Test Creator ID");
        src_dobj.putString( p1, "Simple PT");

        assertFalse( src_dobj.contains(p_creator_id));
        assertTrue( src_dobj.contains(p1));
        assertFalse( src_dobj.contains(t1));

        final ScriptApplicator sa;
        try {
            sa = new ScriptApplicator(bytes( script));
            sa.apply(src_dobj);

        } catch (MizerException e) {
            fail("Unexpected exception: " + e);
        }

        assertEquals( "pn", src_dobj.getString( 0x00100010));
//        assertFalse( src_dobj.contains(p_creator_id));
        assertTrue( src_dobj.contains(p1));
        assertTrue( src_dobj.contains(t1));
        assertEquals( "Simple PT", src_dobj.getString( t1));
    }

    @Test
    public void testAddCreatorID() {

        String script =
                "(0010,0010) := \"pn\" \n"
                        + "set[ \"(0021,0010)\", \"PT Test Creator ID\"] \n"
                        + "(0010,0020) := (0021,{PT Test Creator ID}10)";

        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        // normal private tag
        int[] p_creator_id = {0x00210010};
        int[] p1 = {0x00211010};
        int t1 = 0x00100020;

//        src_dobj.putString( p_creator_id, "PT Test Creator ID");
        src_dobj.putString( p1, "Simple PT");

        assertFalse( src_dobj.contains(p_creator_id));
        assertTrue( src_dobj.contains(p1));
        assertFalse( src_dobj.contains(t1));

        final ScriptApplicator sa;
        try {
            sa = new ScriptApplicator(bytes( script));
            sa.apply(src_dobj);

        } catch (MizerException e) {
            fail("Unexpected exception: " + e);
        }

        assertTrue( src_dobj.contains(p_creator_id));
        assertTrue( src_dobj.contains(p1));
        assertTrue( src_dobj.contains(t1));
        assertEquals( "Simple PT", src_dobj.getString( t1));
    }

    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

}
