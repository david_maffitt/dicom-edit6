/*
 * DicomEdit: TestDeleteStatement
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.test.workers.resources.ResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.PrintStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Run tests on Delete statements
 *
 * Attempts to create adequate coverage of the following features:
 *
 * 1. Public and Private Tags
 * 2. Sequences of Public and Private Tags nested to arbitrary levels.
 * 3. Singular Tag Paths. i.e. paths that map to a single element
 * 4. Plural Tag Paths.  i.e. paths with wildcards that map to multiple elements
 *
 * Created by drm on 8/2/16.
 */
public class TestDeleteStatement {

    private static final Logger logger = LoggerFactory.getLogger(TestDeleteStatement.class);

    private static final ResourceManager _resourceManager = ResourceManager.getInstance();

    private static final File f1 = _resourceManager.getTestResourceFile("dicom/seq/000.dcm");
    private static final File de11 = _resourceManager.getTestResourceFile("dicom/IM_0001");

    private static final String S_DELETE = "-(0010,0020)\n";
    private static final String S_DELETE_W_INLINE_COMMENT = "-(0010,0020) // inline comment\n";
    private static final String S_DELETE_W_TERMINAL_COMMENT = "-(0010,0020)\n// inline comment\n";
    private static final String S_DELETE_PVT = "-(0013,{CTP}10)\n";
    private static final String S_DELETE_ENTIRE_SEQ = "-(0012,0064)\n";
    private static final String S_DELETE_SEQ_ITEM = "-(0012,0064)[0]\n";
    private static final String S_DELETE_ELEMENT_IN_SEQ_ITEM = "-(0012,0064)[1]/(0008,0104)\n";
    private static final String S_DELETE_ELEMENT_IN_ALL_SEQ_ITEM = "-(0012,0064)[%]/(0008,0104)\n";
    private static final String S_DELETE_ELEMENT_EVERYWHERE = "- * / (0010, 0010)\n";
    private static final String S_DELETE_ELEMENT_BELOW_FIRST_LEVEL = "- + / (0010, 0010)\n";
    private static final String S_DELETE_ELEMENT_ONLY_SECOND_LEVEL = "- . / (0010, 0010)\n";

    @Test
    public void testDeleteSingleTag() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f1);
        assertTrue(src_dobj.contains(0x00100020));
        final ScriptApplicator s_delete    = new ScriptApplicator(bytes(S_DELETE));
        final DicomObjectI result_dobj = s_delete.apply(src_dobj);
        assertFalse(result_dobj.contains(0x00100020));
    }

    @Test
    public void testDeleteSingleTagWithInlineComment() throws Exception {
        final DicomObjectI source = DicomObjectFactory.newInstance(f1);
        assertTrue(source.contains(0x00100020));

        final ScriptApplicator deleteWithInline    = new ScriptApplicator(bytes(S_DELETE_W_INLINE_COMMENT));
        final DicomObjectI inlineResult = deleteWithInline.apply(f1);

        final ScriptApplicator deleteWithTerminal = new ScriptApplicator(bytes(S_DELETE_W_TERMINAL_COMMENT));
        final DicomObjectI terminalResult = deleteWithTerminal.apply(f1);

        assertFalse(inlineResult.contains(0x00100020));
        assertFalse(terminalResult.contains(0x00100020));
    }

    @Test
    public void testDeleteSinglePvtTag() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f1);
        assertTrue(src_dobj.contains(0x00131010));
        final ScriptApplicator s_delete = new ScriptApplicator(bytes(S_DELETE_PVT));
        final DicomObjectI result_dobj = s_delete.apply(src_dobj);
        assertFalse(result_dobj.contains(0x00131110));
    }

    @Test
    public void testDeleteSeqAtRoot() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f1);
        logger.info( src_dobj.toString());
        assertTrue(src_dobj.contains(0x00120064));
        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_ENTIRE_SEQ));
        final DicomObjectI result_dobj = sa.apply(src_dobj);
        assertFalse(result_dobj.contains(0x00120064));
        logger.info( result_dobj.toString());
    }

    @Test
    public void testDeleteSeqItem() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f1);
        logger.info( src_dobj.toString());
        int[] s = {0x00120064};
        assertTrue( src_dobj.contains(s));
        int[] s00 = {0x00120064,0,0x00080100};
        assertTrue( src_dobj.contains(s00));
        int[] s01 = {0x00120064,0,0x00080102};
        assertTrue( src_dobj.contains(s01));
        int[] s02 = {0x00120064,0,0x00080103};
        assertTrue( src_dobj.contains(s02));
        int[] s03 = {0x00120064,0,0x00080104};
        assertTrue( src_dobj.contains(s03));
        int[] s10 = {0x00120064,1,0x00080100};
        assertTrue( src_dobj.contains(s10));
        int[] s11 = {0x00120064,1,0x00080102};
        assertTrue( src_dobj.contains(s11));
        int[] s12 = {0x00120064,1,0x00080103};
        assertTrue( src_dobj.contains(s12));
        int[] s13 = {0x00120064,1,0x00080104};
        assertTrue( src_dobj.contains(s13));

        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_SEQ_ITEM));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue( result_dobj.contains(s));
        assertTrue( result_dobj.contains(s00));
        assertTrue( result_dobj.contains(s01));
        assertTrue( result_dobj.contains(s02));
        assertTrue( result_dobj.contains(s03));
        assertFalse( result_dobj.contains(s10));
        assertFalse( result_dobj.contains(s11));
        assertFalse( result_dobj.contains(s12));
        assertFalse( result_dobj.contains(s13));
        logger.info( result_dobj.toString());
    }

    @Test
    public void testDeleteSeqItemElement() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f1);
        logger.info(src_dobj.toString());
        int[] s = {0x00120064};
        assertTrue( src_dobj.contains(s));
        int[] s00 = {0x00120064,0,0x00080100};
        assertTrue( src_dobj.contains(s00));
        int[] s01 = {0x00120064,0,0x00080102};
        assertTrue( src_dobj.contains(s01));
        int[] s02 = {0x00120064,0,0x00080103};
        assertTrue( src_dobj.contains(s02));
        int[] s03 = {0x00120064,0,0x00080104};
        assertTrue( src_dobj.contains(s03));
        int[] s10 = {0x00120064,1,0x00080100};
        assertTrue( src_dobj.contains(s10));
        int[] s11 = {0x00120064,1,0x00080102};
        assertTrue( src_dobj.contains(s11));
        int[] s12 = {0x00120064,1,0x00080103};
        assertTrue( src_dobj.contains(s12));
        int[] s13 = {0x00120064,1,0x00080104};
        assertTrue( src_dobj.contains(s13));

        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_ELEMENT_IN_SEQ_ITEM));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue( result_dobj.contains(s));
        assertTrue( result_dobj.contains(s00));
        assertTrue( result_dobj.contains(s01));
        assertTrue( result_dobj.contains(s02));
        assertTrue( result_dobj.contains(s03));
        assertTrue( result_dobj.contains(s10));
        assertTrue( result_dobj.contains(s11));
        assertTrue( result_dobj.contains(s12));
        assertFalse( result_dobj.contains(s13));
        logger.info(result_dobj.toString());
    }

    @Test
    public void testDE11() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(de11);
        logger.info(src_dobj.toString());
        int[] s = {0x00080100};
        assertTrue( src_dobj.contains(s));
        int[] s1 = {0x00180026,0,0x00180029,0,0x00080100};
        assertTrue( src_dobj.contains(s1));
        int[] s2 = {0x00400260,0,0x00080100};
        assertTrue( src_dobj.contains(s2));
        int[] s3 = {0x20051402,0,0x00080100};
        assertTrue( src_dobj.contains(s3));

        final ScriptApplicator sa = new ScriptApplicator(bytes("- * / (0008, 0100)\n"));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertFalse( result_dobj.contains(s));
        assertFalse( result_dobj.contains(s1));
        assertFalse( result_dobj.contains(s2));
        assertFalse( result_dobj.contains(s3));
        logger.info(result_dobj.toString());
    }

    @Test
    public void testDeleteSeqAllItemElement() throws Exception {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f1);
        logger.info(src_dobj.toString());
        int[] s = {0x00120064};
        assertTrue( src_dobj.contains(s));
        int[] s00 = {0x00120064,0,0x00080100};
        assertTrue( src_dobj.contains(s00));
        int[] s01 = {0x00120064,0,0x00080102};
        assertTrue( src_dobj.contains(s01));
        int[] s02 = {0x00120064,0,0x00080103};
        assertTrue( src_dobj.contains(s02));
        int[] s03 = {0x00120064,0,0x00080104};
        assertTrue( src_dobj.contains(s03));
        int[] s10 = {0x00120064,1,0x00080100};
        assertTrue( src_dobj.contains(s10));
        int[] s11 = {0x00120064,1,0x00080102};
        assertTrue( src_dobj.contains(s11));
        int[] s12 = {0x00120064,1,0x00080103};
        assertTrue( src_dobj.contains(s12));
        int[] s13 = {0x00120064,1,0x00080104};
        assertTrue( src_dobj.contains(s13));

        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_ELEMENT_IN_ALL_SEQ_ITEM));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue( result_dobj.contains(s));
        assertTrue( result_dobj.contains(s00));
        assertTrue( result_dobj.contains(s01));
        assertTrue( result_dobj.contains(s02));
        assertFalse( result_dobj.contains(s03));
        assertTrue( result_dobj.contains(s10));
        assertTrue( result_dobj.contains(s11));
        assertTrue( result_dobj.contains(s12));
        assertFalse( result_dobj.contains(s13));
        logger.info(result_dobj.toString());
    }

    @Test
    public void testDeleteElementEverywhere() throws Exception {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance(f2);
        final DicomObjectI src_dobj = createSeqTestObject();

        logger.info(src_dobj.toString());

        assertTrue( src_dobj.contains(s));

        assertTrue( src_dobj.contains(s0a));
        assertTrue( src_dobj.contains(s1a));
        assertTrue( src_dobj.contains(s0b));
        assertTrue( src_dobj.contains(s1b));

        assertTrue( src_dobj.contains(s00));
        assertTrue( src_dobj.contains(s01));
        assertTrue( src_dobj.contains(s10));
        assertTrue( src_dobj.contains(s11));

        assertTrue( src_dobj.contains(s000));
        assertTrue( src_dobj.contains(s001));
        assertTrue( src_dobj.contains(s010));
        assertTrue( src_dobj.contains(s011));
        assertTrue( src_dobj.contains(s100));
        assertTrue( src_dobj.contains(s101));
        assertTrue( src_dobj.contains(s110));
        assertTrue( src_dobj.contains(s111));

        assertTrue( src_dobj.contains(p0a));
        assertTrue( src_dobj.contains(p0b));
        assertTrue( src_dobj.contains(p1a));
        assertTrue( src_dobj.contains(p1b));

        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_ELEMENT_EVERYWHERE));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertFalse( result_dobj.contains(s));

        assertFalse( result_dobj.contains(s0a));
        assertFalse( result_dobj.contains(s1a));
        assertTrue( src_dobj.contains(s0b));
        assertTrue( src_dobj.contains(s1b));

        assertFalse( result_dobj.contains(s00));
        assertFalse( result_dobj.contains(s01));
        assertFalse( result_dobj.contains(s10));
        assertFalse( result_dobj.contains(s11));

        assertFalse( result_dobj.contains(s000));
        assertFalse( result_dobj.contains(s001));
        assertFalse( result_dobj.contains(s010));
        assertFalse( result_dobj.contains(s011));
        assertFalse( result_dobj.contains(s100));
        assertFalse( result_dobj.contains(s101));
        assertFalse( result_dobj.contains(s110));
        assertFalse( result_dobj.contains(s111));

        assertFalse( src_dobj.contains(p0a));
        assertTrue( src_dobj.contains(p0b));
        assertFalse( src_dobj.contains(p1a));
        assertTrue( src_dobj.contains(p1b));

        logger.info(result_dobj.toString());
//        DumpVisitor dv = new DumpVisitor(result_dobj, new PrintStream(new File("/tmp/foo.txt")));
//        dv.visit( result_dobj);
    }

    @Test
    public void testDeleteElementEverywhereBelowFirstLevel() throws Exception {
        final DicomObjectI src_dobj = createSeqTestObject();

        logger.info(src_dobj.toString());

        assertTrue( src_dobj.contains(s));

        assertTrue( src_dobj.contains(s0a));
        assertTrue( src_dobj.contains(s1a));

        assertTrue( src_dobj.contains(s00));
        assertTrue( src_dobj.contains(s01));
        assertTrue( src_dobj.contains(s10));
        assertTrue( src_dobj.contains(s11));

        assertTrue( src_dobj.contains(s000));
        assertTrue( src_dobj.contains(s001));
        assertTrue( src_dobj.contains(s010));
        assertTrue( src_dobj.contains(s011));
        assertTrue( src_dobj.contains(s100));
        assertTrue( src_dobj.contains(s101));
        assertTrue( src_dobj.contains(s110));
        assertTrue( src_dobj.contains(s111));

        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_ELEMENT_BELOW_FIRST_LEVEL));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue( result_dobj.contains(s));

        assertFalse( result_dobj.contains(s0a));
        assertFalse( result_dobj.contains(s1a));

        assertFalse( result_dobj.contains(s00));
        assertFalse( result_dobj.contains(s01));
        assertFalse( result_dobj.contains(s10));
        assertFalse( result_dobj.contains(s11));

        assertFalse( result_dobj.contains(s000));
        assertFalse( result_dobj.contains(s001));
        assertFalse( result_dobj.contains(s010));
        assertFalse( result_dobj.contains(s011));
        assertFalse( result_dobj.contains(s100));
        assertFalse( result_dobj.contains(s101));
        assertFalse( result_dobj.contains(s110));
        assertFalse( result_dobj.contains(s111));

        logger.info(result_dobj.toString());
//        DumpVisitor dv = new DumpVisitor(result_dobj, new PrintStream(new File("/tmp/foo.txt")));
//        dv.visit( result_dobj);
    }

    @Test
    public void testDeleteElementSecondLevelOnly() throws Exception {
        final DicomObjectI src_dobj = createSeqTestObject();

        logger.info(src_dobj.toString());

        assertTrue( src_dobj.contains(s));

        assertTrue( src_dobj.contains(s0a));
        assertTrue( src_dobj.contains(s1a));

        assertTrue( src_dobj.contains(s00));
        assertTrue( src_dobj.contains(s01));
        assertTrue( src_dobj.contains(s10));
        assertTrue( src_dobj.contains(s11));

        assertTrue( src_dobj.contains(s000));
        assertTrue( src_dobj.contains(s001));
        assertTrue( src_dobj.contains(s010));
        assertTrue( src_dobj.contains(s011));
        assertTrue( src_dobj.contains(s100));
        assertTrue( src_dobj.contains(s101));
        assertTrue( src_dobj.contains(s110));
        assertTrue( src_dobj.contains(s111));

        final ScriptApplicator sa = new ScriptApplicator(bytes(S_DELETE_ELEMENT_ONLY_SECOND_LEVEL));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue( result_dobj.contains(s));

        assertFalse( result_dobj.contains(s0a));
        assertFalse( result_dobj.contains(s1a));

        assertTrue( result_dobj.contains(s00));
        assertTrue( result_dobj.contains(s01));
        assertTrue( result_dobj.contains(s10));
        assertTrue( result_dobj.contains(s11));

        assertTrue( result_dobj.contains(s000));
        assertTrue( result_dobj.contains(s001));
        assertTrue( result_dobj.contains(s010));
        assertTrue( result_dobj.contains(s011));
        assertTrue( result_dobj.contains(s100));
        assertTrue( result_dobj.contains(s101));
        assertTrue( result_dobj.contains(s110));
        assertTrue( result_dobj.contains(s111));

        logger.info(result_dobj.toString());
//        DumpVisitor dv = new DumpVisitor(result_dobj, new PrintStream(new File("/tmp/foo.txt")));
//        dv.visit( result_dobj);
    }

    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    private DicomObjectI createSeqTestObject() {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        src_dobj.putString( s, "PatientName");
        int[] foo = {0x00200010};
        src_dobj.putString( foo, "foo");

        src_dobj.putString( s0a, "s0a");
        src_dobj.putString( s1a, "s1a");
        src_dobj.putString( s0b, "s0b");
        src_dobj.putString( s1b, "s1a");
        int[] foo2 = {0x00081032,1,0x00200010};
        src_dobj.putString( foo2, "foo");

        src_dobj.putString( s00, "s00");
        src_dobj.putString( s01, "s01");
        src_dobj.putString( s10, "s10");
        src_dobj.putString( s11, "s11");

        src_dobj.putString( s000, "s000");
        src_dobj.putString( s001, "s001");
        src_dobj.putString( s010, "s010");
        src_dobj.putString( s011, "s011");
        src_dobj.putString( s100, "s100");
        src_dobj.putString( s101, "s101");
        src_dobj.putString( s110, "s110");
        src_dobj.putString( s111, "s111");

        src_dobj.putString( pcreator, "pcreator");
        src_dobj.putString( p, "p");

        src_dobj.putString( p0a, "p0a");
        src_dobj.putString( p0b, "p0b");
        src_dobj.putString( p1a, "p1a");
        src_dobj.putString( p1b, "p1b");

        return src_dobj;
    }

    private static int[] s = {0x00100010};

    private static int[] s0a = {0x00081032,0,0x00100010};
    private static int[] s0b = {0x00081032,0,0x00100020};
    private static int[] s1a = {0x00081032,1,0x00100010};
    private static int[] s1b = {0x00081032,1,0x00100020};

    private static int[] s00 = {0x00081032,0,0x00201032,0,0x00100010};
    private static int[] s01 = {0x00081032,0,0x00201032,1,0x00100010};
    private static int[] s10 = {0x00081032,1,0x00201033,0,0x00100010};
    private static int[] s11 = {0x00081032,1,0x00201033,1,0x00100010};

    private static int[] s000 = {0x00081032,0,0x00201032,0,0x00201034,0,0x00100010};
    private static int[] s001 = {0x00081032,0,0x00201032,0,0x00201034,1,0x00100010};
    private static int[] s010 = {0x00081032,0,0x00201032,1,0x00201036,0,0x00100010};
    private static int[] s011 = {0x00081032,0,0x00201032,1,0x00201036,1,0x00100010};
    private static int[] s100 = {0x00081032,1,0x00201033,0,0x00201035,0,0x00100010};
    private static int[] s101 = {0x00081032,1,0x00201033,0,0x00201035,1,0x00100010};
    private static int[] s110 = {0x00081032,1,0x00201033,1,0x00201037,0,0x00100010};
    private static int[] s111 = {0x00081032,1,0x00201033,1,0x00201037,1,0x00100010};

    private static int[] pcreator = {0x00130010};
    private static int[] p = {0x00131000};

    private static int[] p0a = {0x00131010,0,0x00100010};
    private static int[] p0b = {0x00131010,0,0x00100011};
    private static int[] p1a = {0x00131010,1,0x00100010};
    private static int[] p1b = {0x00131010,1,0x0010001b};

}
