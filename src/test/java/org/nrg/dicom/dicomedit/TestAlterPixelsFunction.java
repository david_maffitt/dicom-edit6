/*
 * DicomEdit: TestFunctions
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Ignore;
import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationRuntimeException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.test.workers.resources.ResourceManager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;

/**
 * Run tests of alterPixels function.
 *
 */
public class TestAlterPixelsFunction {
    private static final ResourceManager _resourceManager = ResourceManager.getInstance();

    private static final File FILE4 = _resourceManager.getTestResourceFile("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz");

    /**
     * Test script with supported pixel editor.
     *
     * Writes output in /tmp.  Currently need to open the image in a viewer to confirm it looks as expected.
     */
    @Test
    @Ignore
    public void testPixelEdit() {
        try {
            String script =
                    "alterPixels[\"rectangle\", \"l=100, t=100, r=200, b=200\", \"solid\", \"v=100\"] \n";

            ClassLoader loader = TestAlterPixelsFunction.class.getClassLoader();
            File file = new File( loader.getResource("dicom/US-evle-mono2-8bits.dcm").toURI());
            final DicomObjectI src_dobj = DicomObjectFactory.newInstance( file);

            final ScriptApplicator sa = new ScriptApplicator( bytes(script));
            final DicomObjectI result_dobj = sa.apply(src_dobj);

            Path f = Files.createTempFile(Paths.get("/tmp"),"de6", ".dcm");
            try (FileOutputStream os = new FileOutputStream(f.toFile())) {
                result_dobj.write(os);
            }
        }
        catch(IOException | MizerException | URISyntaxException e) {
            fail("Unexpected exception: " + e);
        }
    }

    @Test
    @Ignore
    public void testSupportedEditor() {
        try {
            String script =
                    "alterPixels[\"rectangle\", \"l=100, t=100, r=200, b=200\", \"solid\", \"v=100\"] \n";

            final DicomObjectI src_dobj = DicomObjectFactory.newInstance(FILE4);

            final ScriptApplicator sa = new ScriptApplicator( bytes(script));
            final DicomObjectI result_dobj = sa.apply(src_dobj);

            Path f = Files.createTempFile(Paths.get("/tmp"),"de6", ".dcm");
            try (FileOutputStream os = new FileOutputStream(f.toFile())) {
                result_dobj.write(os);
            }
        }
        catch( IOException | MizerException e) {
            fail("Unexpected exception: " + e);
        }
    }

    /**
     * Test script with un-supported pixel editor.
     */
    @Test
    @Ignore
    public void testUnSupportedEditor() {
        try {
            String script =
                    "alterPixels[\"foo\", \"l=100, t=100, r=200, b=200\", \"solid\", \"v=100\"] \n";

            final DicomObjectI src_dobj = DicomObjectFactory.newInstance(FILE4);

            final ScriptApplicator sa = new ScriptApplicator( bytes(script));
            final DicomObjectI result_dobj = sa.apply(src_dobj);

            Path f = Files.createTempFile(Paths.get("/tmp"),"de6", ".dcm");
            try (FileOutputStream os = new FileOutputStream(f.toFile())) {
                result_dobj.write(os);
            }
            fail();
        }
        catch( ScriptEvaluationRuntimeException e) {
            assertTrue( getRootCause(e) instanceof UnsupportedOperationException);
        }
        catch( Exception e) {
            fail("Unexpected exception: " + e);
        }
    }

    /**
     * Test script with bad parameter syntax.
     */
    @Test
    @Ignore
    public void testTooFewParams() {
        try {
            String script =
                    "alterPixels[\"rectangle\", \"l=100, t=100, r=200, b=200\", \"solid\" ]\n";

            final DicomObjectI src_dobj = DicomObjectFactory.newInstance(FILE4);

            final ScriptApplicator sa = new ScriptApplicator( bytes(script));
            final DicomObjectI result_dobj = sa.apply(src_dobj);

            Path f = Files.createTempFile(Paths.get("/tmp"),"de6", ".dcm");
            try (FileOutputStream os = new FileOutputStream(f.toFile())) {
                result_dobj.write(os);
            }
            fail();
        }
        catch( ScriptEvaluationRuntimeException e) {
            assertTrue( getRootCause(e) instanceof ScriptEvaluationException);
        }
        catch( Exception e) {
            fail("Unexpected exception: " + e);
        }
    }

    public Throwable getRootCause(Throwable throwable) {
        Objects.requireNonNull(throwable);
        Throwable rootCause = throwable;
        while (rootCause.getCause() != null && rootCause.getCause() != rootCause) {
            rootCause = rootCause.getCause();
        }
        return rootCause;
    }

    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

}
