package org.nrg.dicom.dicomedit.mizer;

import org.junit.Test;
import org.nrg.dicom.dicomedit.mizer.DE6Mizer;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

public class TestPersistentContext {

    @Test
    public void testPersistentContext() {
        try {
            DE6Mizer mizer = new DE6Mizer();

            List<String> script = new ArrayList<>();
            script.add("version \"6.1\"");
            script.add("(0020,000E) := mapUID[(0020,000E), \"6.666\"]");

            MizerContextWithScript context = createContext( "ProjectA", "SubjectA", "SessionA", 0l, script, false);

            mizer.setContext( context);
        } catch (MizerException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    public MizerContextWithScript createContext( final String project, final String subject, final String session, final long scriptId, final Object script, final boolean record) throws MizerException {
        final MizerContextWithScript mizerContext = new MizerContextWithScript();
        mizerContext.setScriptId(scriptId);
        mizerContext.setElement("project", project);
        mizerContext.setElement("subject", subject);
        mizerContext.setElement("session", session);
        if (script instanceof List) {
            //noinspection unchecked
            mizerContext.setScript((List<String>) script);
        } else if (script instanceof InputStream) {
            mizerContext.setScript((InputStream) script);
        } else {
            mizerContext.setScript(script.toString());
        }
        mizerContext.setRecord(record);
        return mizerContext;
    }

}
