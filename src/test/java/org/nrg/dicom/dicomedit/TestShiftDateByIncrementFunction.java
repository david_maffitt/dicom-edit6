/*
 * DicomEdit: TestFunctions
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;

/**
 * Run tests of shiftDateByIncrement function.
 *
 */
public class TestShiftDateByIncrementFunction {

    /**
     * Test date shift in simple tags.
     *
     * @throws MizerException
     */
    @Test
    public void testSimpleTags() throws MizerException {

        String script =
                "(0008,0020) := shiftDateByIncrement[ (0008,0020), \"14\"]\n" +
                        "(0008,0021) := shiftDateByIncrement[ (0008,0021), \"14\"]";

        final DicomObjectI src_dobj = createTestObject();

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( "20120919", src_dobj.getString(studyDate));
        assertEquals( "20131231", src_dobj.getString(seriesDate));

        final ScriptApplicator sa = new ScriptApplicator(bytes(script));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( "20121003", result_dobj.getString(studyDate));
        assertEquals( "20140114", result_dobj.getString(seriesDate));
    }

    @Test
    public void test5YearShiftTags() throws MizerException {
        // Note date changes because of leap year.
        // 5 * 365 = 1825
        String script =
                "(0008,0020) := shiftDateByIncrement[ (0008,0020), \"1825\"]\n" ;

        final DicomObjectI src_dobj = createTestObject();

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( "20120919", src_dobj.getString(studyDate));
        assertEquals( "20131231", src_dobj.getString(seriesDate));

        final ScriptApplicator sa = new ScriptApplicator(bytes(script));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( "20170918", result_dobj.getString(studyDate));
    }


    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    private DicomObjectI createTestObject() {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        src_dobj.putString(patientName, "PatientName");

        src_dobj.putString(studyDate, "20120919");
        src_dobj.putString(seriesDate, "20131231");

        return src_dobj;
    }

    private static int[] patientName = {0x00100010};

    private static int[] studyDate = {0x00080020};
    private static int[] seriesDate = {0x00080021};

}
