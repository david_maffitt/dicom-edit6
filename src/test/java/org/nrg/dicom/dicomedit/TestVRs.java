/*
 * DicomEdit: TestVRs
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.dcm4che2.util.TagUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.exceptions.MizerException;

import java.io.*;

import static org.junit.Assert.assertEquals;

/**
 * Run tests of assignments to all the different VRs.
 *
 * Write it out, read it in.  Any surprises?
 *
 */
public class TestVRs {
    public static final int TAG_AE = 0x21000140;   public static final String AE_VALUE = "seventeencharacte";  // Destination AE
    public static final int TAG_AS = 0x00101010;   public static final String AS_VALUE = "056Y"; // PatientAge
    public static final int TAG_AT = 0x00280808;   public static final String AT_VALUE = "00100010"; // ImageDataLocation
    public static final int TAG_CS = 0x00280040;   public static final String CS_VALUE = "CODE_STRING"; // ImageFormat
    public static final int TAG_DA = 0x00080012;   public static final String DA_VALUE = "19600519"; // InstanceCreationDate
    public static final int TAG_DS = 0x30060044;   public static final String DS_VALUE = " -3.14159"; // ContourSlabThickness
    public static final int TAG_DT = 0x0040A120;   public static final String DT_VALUE = "20160825225530.23+0600"; // DateTime
    public static final int TAG_FL = 0x00700262;   public static final String FL_VALUE = "3.333"; // DiameterOfVisibility
    public static final int TAG_FD = 0x00189087;   public static final String FD_VALUE = "3.333333"; // Difusion b-value
    public static final int TAG_IS = 0x00183105;   public static final String IS_VALUE = "-666"; // Lesion Number
    public static final int TAG_LO = 0x300A033A;   public static final String LO_VALUE = "Long string."; // LateralSpreadingDeviceDescription
    public static final int TAG_LT = 0x00184000;   public static final String LT_VALUE = "Looooong text."; // AcquistionComments
    public static final int TAG_OB = 0x04000520;   public static final String OB_VALUE = "New Institute"; // Encrypted content
//    public static final int TAG_OD = 0x?  ;        public static final String OD_VALUE = "New Institute"; // none?
    public static final int TAG_OF = 0x00660016;   public static final String OF_VALUE = "New Institute"; // PointsCoordinateData
    public static final int TAG_OW = 0x00660023;   public static final String OW_VALUE = "New Institute"; // TrianglePointIndexList
    public static final int TAG_SH = 0x00181160;   public static final String SH_VALUE = "New Institute"; // FilterType
    public static final int TAG_SL = 0x00186020;   public static final String SL_VALUE = "New Institute"; // ReferencePixelX0
    public static final int TAG_SQ = 0x300A00B0;   public static final String SQ_VALUE = "New Institute"; // BeamSequence
    public static final int TAG_SS = 0x300A0306;   public static final String SS_VALUE = "New Institute"; // RadiationChargeState
    public static final int TAG_ST = 0x00080092;   public static final String ST_VALUE = "New Institute"; // ReferringPhysicianAddress
    public static final int TAG_TM = 0x00100032;   public static final String TM_VALUE = "New Institute"; // PatientBirthTime
    public static final int TAG_UI = 0x0020000E;   public static final String UI_VALUE = "New Institute"; // SeriesInstanceUID
    public static final int TAG_UL = 0x00081161;   public static final String UL_VALUE = "New Institute"; // SimpleFrameList
    public static final int TAG_UN = 0x66666666;   public static final String UN_VALUE = "New Institute"; // Unknown Tag
    public static final int TAG_US = 0x00280106;   public static final String US_VALUE = "New Institute"; // SmallestImagePixelValue
    public static final int TAG_UT = 0x00400602;   public static final String UT_VALUE = "New Institute"; // SpecimenDetailedDescription

    public String assignmentStatement( int tag, String value) {
        return String.format("%s := \"%s\"", TagUtils.toString(tag), value);
    }

    @Test
    public void testAssignToAEFromString() throws MizerException {
        assignFromString( TAG_AE, AE_VALUE);
    }

    @Test
    public void testAssignToASFromString() throws MizerException {
        assignFromString( TAG_AS, AS_VALUE);
   }

    /**
     * Test writing/reading VR AT (Attribute Tag) from String.
     *
     * fails because I haven't found the correct string syntax to specify the tag.  It might not be possible from String.
     *
     * (0028,0808) AT #8 [30303031\30303031] Image Data Location
     * Expected :30303031
     * Actual   :00100010
     *
     * @throws MizerException When an error occurs processing the DICOM object.
     */
    @Test
    @Ignore
    public void testAssignToATFromString() throws MizerException {
        assignFromString( TAG_AT, AT_VALUE);
    }

    /**
     * Test writing/reading to UNKNOWN VR from String.
     *
     * fails because
     *  org.junit.ComparisonFailure:
     *  Expected :New Institute\00
     *  Actual   :New Institute
     *  The length is padded to be of even length when written and reading doesn't know the real intended length.
     */
    @Test
    @Ignore
    public void testAssignToUNFromString() throws MizerException {
        assignFromString( TAG_UN, UN_VALUE);
    }

    public void assignFromString(int tag, String value) throws MizerException {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        final ScriptApplicator sa          = new ScriptApplicator(bytes(assignmentStatement(tag, value)));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        result_dobj.addMetaHeader();
        try {
            final File file = File.createTempFile("dcmTest", "dcm");
            try (final OutputStream os = new FileOutputStream(file)) {
                result_dobj.write(os);
            }

            try ( FileInputStream is = new FileInputStream( file)) {
                final DicomObjectI dicomObject = DicomObjectFactory.newInstance(is);
                assertEquals( dicomObject.getString(tag), value);
            }
        } catch (IOException e) {
            throw new MizerException(e);
        }
    }

    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

}
