/*
 * DicomEdit: TestAssignmentStatement
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.tags.TagPath;
import org.nrg.dicom.mizer.tags.TagPrivate;
import org.nrg.dicom.mizer.tags.TagPublic;
import org.nrg.dicom.mizer.tags.TagSequence;
import org.nrg.dicom.mizer.variables.Variable;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.*;

/**
 * Run tests of assignment/initialization statements.
 *
 * Assignment/Initialization statements have syntax: lvalue := value
 * lvalue is a tagpath or a variable. Tagpaths are singular or plural-valued (have wildcards). Assigning to a
 * plural-valued tagpath is an error.
 * value is one of String, number, tagpath, function or variable. Tagpaths are singular or pluraled valued. Assigning
 * from a plural-valued tagpath is an error.
 *
 * Created by drm on 8/8/16.
 */
public class TestAssignIfExistsStatement {
    public static final String ASSIGN_TO_PUBLIC_TAG_FROM_STRING = "(0010,0010) ?= \"Fine^Curly\" \n";
    public static final String ASSIGN_TO_PVT_TAG_FROM_STRING = "(0019,{GE}10) ?= \"Edited GE pvt tag\" \n";
    public static final String ASSIGN_TO_PUBLIC_TAG_IN_SEQ_FROM_STRING = "(0008,1032)[1]/(0010,0010) := \"Assigned string.\" \n";
    public static final String ASSIGN_TO_PVT_TAG_IN_SEQ_FROM_STRING = "(0021,{PVT ID}01)[1]/(0010,0010) := \"Assigned string.\" \n";
    public static final String ASSIGN_TO_PLURAL_TAG_FROM_STRING = "(002#,{PVT}01)[1]/(0010,0010) := \"Assigned string.\"\n";
    public static final String ASSIGN_TO_VARIABLE_FROM_PUBLIC_TAG = "foo := (0010,0010)";
    public static final String ASSIGN_FROM_STRING_TO_PUBLIC_TAG = "(0010,0010) := \"snafu\" \n";
    public static final String ASSIGN_FROM_NUMBER_TO_PUBLIC_TAG = "(0010,0010) := 666\n";
    public static final String ASSIGN_FROM_SINGULAR_TAGPATH_TO_PUBLIC_TAG = "(0010,0010) := (0008,0020)\n";
    public static final String ASSIGN_FROM_PLURAL_TAGPATH_TO_PUBLIC_TAG = "(0010,0010) := (0008,002X)\n";
    public static final String ASSIGN_FROM_FUNCTION_TO_PUBLIC_TAG = "(0010,0010) := concatenate[ \"situation normal \", (0008,0020)]\n";

    // 'To' tests.  Test storing the value in all permutations of the left side of the assignment operator.
    @Test
    public void testAssignToNonExistingPublicTagFromString() throws MizerException {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        int[] s = {0x00080080};
        src_dobj.putString( s, "Some Institution");

        assertTrue(src_dobj.contains(0x00080080));
        assertEquals( src_dobj.getString(s), "Some Institution");
        assertFalse(src_dobj.contains(0x00100010));

        final ScriptApplicator sa          = new ScriptApplicator(bytes(ASSIGN_TO_PUBLIC_TAG_FROM_STRING));
        final DicomObjectI      result_dobj = sa.apply(src_dobj);

        assertTrue(src_dobj.contains(0x00080080));
        assertEquals( src_dobj.getString(s), "Some Institution");
        assertFalse(src_dobj.contains(0x00100010));
    }

    @Test
    public void testAssignToExistingPublicTagFromString() throws MizerException {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        int[] s = {0x00080080};
        src_dobj.putString( s, "Some Institution");

        int[] p = {0x00100010};
        src_dobj.putString( p, "Howard^Moe");

        assertTrue(src_dobj.contains(0x00080080));
        assertEquals( src_dobj.getString(s), "Some Institution");
        assertTrue(src_dobj.contains(0x00100010));
        assertEquals( src_dobj.getString(p), "Howard^Moe");

        final ScriptApplicator sa          = new ScriptApplicator(bytes(ASSIGN_TO_PUBLIC_TAG_FROM_STRING));
        final DicomObjectI      result_dobj = sa.apply(src_dobj);

        assertTrue(src_dobj.contains(0x00080080));
        assertEquals( src_dobj.getString(s), "Some Institution");
        assertTrue(src_dobj.contains(0x00100010));
        assertEquals( src_dobj.getString(p), "Fine^Curly");
    }

//    @Test
//    public void testAssignToExistingPrivateTagFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int tge = src_dobj.putString( 0x00191010, "GE", "GE private string");
//        int tsm = src_dobj.putString( 0x00191010, "SIEMENS", "SIEMENS private string");
//        assertEquals( src_dobj.getString(tge), "GE private string");
//        assertEquals( src_dobj.getString(tsm), "SIEMENS private string");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PVT_TAG_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(tge), "GE private string");
//        assertEquals( result_dobj.getString(tsm), "Edited SIEMENS pvt tag");
//    }
//
    @Test
    public void testAssignToNotExistingPrivateTagFromString() throws MizerException {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        int[] p = {0x00100010};
        src_dobj.putString( p, "Howard^Moe");

        assertTrue(src_dobj.contains(0x00100010));
        assertEquals( src_dobj.getString(p), "Howard^Moe");
        assertFalse(src_dobj.contains(0x00191010));

    //        int tge = src_dobj.putString( 0x00191010, "GE", "GE private string");
    //        assertEquals( src_dobj.getString(tge), "GE private string");
    //        TagPath tp = new TagPath();
    //        tp.addTag( new TagPrivate(0x00191010, "SIEMENS"));
    //
    //        assertNull( src_dobj.getString(tp));
    //
        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PVT_TAG_FROM_STRING));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue(src_dobj.contains(0x00100010));
        assertEquals( src_dobj.getString(p), "Howard^Moe");
        assertFalse(src_dobj.contains(0x00191010));
    }

    @Test
    public void testAssignToExistingPrivateTagFromString() throws MizerException {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        int[] p = {0x00100010};
        src_dobj.putString( p, "Howard^Moe");

        assertTrue(src_dobj.contains(0x00100010));
        assertEquals( src_dobj.getString(p), "Howard^Moe");
        assertFalse(src_dobj.contains(0x00191010));

        int tge = src_dobj.putString( 0x00191010, "GE", "GE private string");
        assertEquals( src_dobj.getString(tge), "GE private string");

        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PVT_TAG_FROM_STRING));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertTrue(src_dobj.contains(0x00100010));
        assertEquals( src_dobj.getString(p), "Howard^Moe");
        assertTrue(src_dobj.contains(0x00191010));
        assertEquals( src_dobj.getString(0x00191010), "Edited GE pvt tag");
    }

//    @Test
//    public void testAssignToNotExistingPrivateTagFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int tge = src_dobj.putString( 0x00191010, "GE", "GE private string");
//        assertEquals( src_dobj.getString(tge), "GE private string");
//        TagPath tp = new TagPath();
//        tp.addTag( new TagPrivate(0x00191010, "SIEMENS"));
//
//        assertNull( src_dobj.getString(tp));
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PVT_TAG_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(tge), "GE private string");
//        assertEquals( result_dobj.getString(tp), "Edited SIEMENS pvt tag");
//    }
//
//    @Test
//    public void testAssignToExistingPublicTagInSequenceFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s0 = {0x00081032,0,0x00100010};
//        int[] s1 = {0x00081032,1,0x00100010};
//        src_dobj.putString( s0, "s0");
//        src_dobj.putString( s1, "s1");
//
//        assertEquals( src_dobj.getString(s0), "s0");
//        assertEquals( src_dobj.getString(s1), "s1");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PUBLIC_TAG_IN_SEQ_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(s0), "s0");
//        assertEquals( result_dobj.getString(s1), "Assigned string.");
//    }
//
//    @Test
//    public void testAssignToNotExistingPublicTagInSequenceFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s0 = {0x00081032,0,0x00100010};
//        int[] s1 = {0x00081032,1,0x00100010};
//        src_dobj.putString( s0, "s0");
//
//        assertEquals( src_dobj.getString(s0), "s0");
//        assertNull( src_dobj.getString(s1));
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PUBLIC_TAG_IN_SEQ_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(s0), "s0");
//        assertEquals( result_dobj.getString(s1), "Assigned string.");
//    }
//
//    @Test
//    public void testAssignToExistingPrivateTagInSequenceFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        TagPath tp1 = new TagPath();
//        tp1.addTag( new TagSequence(new TagPrivate(0x00211001, "PVT ID"), 0)).addTag(new TagPublic(0x00100010));
//        TagPath tp2 = new TagPath();
//        tp2.addTag( new TagSequence( new TagPrivate( 0x00211001, "PVT ID"), 1)).addTag( new TagPublic( 0x00100010));
//        src_dobj.assign( tp1, "s0");
//        src_dobj.assign( tp2, "s1");
//
//        int[] s  = {0x00210010};
//        int[] s0 = {0x00211001,0,0x00100010};
//        int[] s1 = {0x00211001,1,0x00100010};
//        assertEquals( src_dobj.getString(s), "PVT ID");
//        assertEquals( src_dobj.getString(s0), "s0");
//        assertEquals( src_dobj.getString(s1), "s1");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PVT_TAG_IN_SEQ_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(s), "PVT ID");
//        assertEquals( result_dobj.getString(s0), "s0");
//        assertEquals( result_dobj.getString(s1), "Assigned string.");
//    }
//
//    @Test
//    public void testAssignToNotExistingPrivateTagInSequenceFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
////        TagPath tp1 = new TagPath();
////        tp1.addTag( new TagSequence( new TagPrivate( 0x00211001, "PVT"), 0)).addTag( new TagPublic( 0x00100010));
////        TagPath tp2 = new TagPath();
////        tp2.addTag( new TagSequence( new TagPrivate( 0x00211001, "PVT"), 1)).addTag( new TagPublic( 0x00100010));
////        src_dobj.assign( tp1, "s0");
////        src_dobj.assign( tp2, "s1");
//
//        int[] s  = {0x00210010};
//        int[] s0 = {0x00211001,0,0x00100010};
//        int[] s1 = {0x00211001,1,0x00100010};
//        assertNull( src_dobj.getString(s));
//        assertNull( src_dobj.getString(s0));
//        assertNull( src_dobj.getString(s1));
////        public static final String ASSIGN_TO_PVT_TAG_IN_SEQ_FROM_STRING = "(0021,{PVT ID}01)[1]/(0010,0010) := \"Assigned string.\" \n";
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PVT_TAG_IN_SEQ_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(s), "PVT ID");
//        assertNull( result_dobj.getString(s0));
//        assertEquals( result_dobj.getString(s1), "Assigned string.");
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void testAssignToNonSingularPublicTagPathFromString() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_PLURAL_TAG_FROM_STRING));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//    }
//
//    @Test
//    public void testAssignToVariableFromPublicTag() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s = {0x00100010};
//        src_dobj.putString( s, "Patient^Name");
//
//        assertTrue(src_dobj.contains(0x00100010));
//        assertEquals( src_dobj.getString(s), "Patient^Name");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_TO_VARIABLE_FROM_PUBLIC_TAG));
//
//        final Variable variable = sa.getVariable("foo");
//        assertNull(variable);
//
//        sa.apply(src_dobj);
//
//        assertEquals( sa.getValue("foo").asString(), "Patient^Name");
//    }
//
//
//    // 'From' tests. Test getting the correct values from the right side of the assignment op.
//    @Test
//    public void testAssignFromStringToPublicTag() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s = {0x00100010};
//        src_dobj.putString( s, "Patient^Name");
//
//        assertTrue(src_dobj.contains(0x00100010));
//        assertEquals( src_dobj.getString(s), "Patient^Name");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_FROM_STRING_TO_PUBLIC_TAG));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(0x00100010), "snafu");
//    }
//
//    @Test
//    public void testAssignFromNumberToPublicTag() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s = {0x00100010};
//        src_dobj.putString( s, "Patient^Name");
//
//        assertTrue(src_dobj.contains(0x00100010));
//        assertEquals( src_dobj.getString(s), "Patient^Name");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_FROM_NUMBER_TO_PUBLIC_TAG));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(0x00100010), Integer.toString(666));
//    }
//
//    @Test
//    public void testAssignFromSingularTagPathToPublicTag() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s1 = {0x00100010};
//        src_dobj.putString( s1, "Patient^Name");
//        int[] s2 = {0x00080020};
//        src_dobj.putString( s2, "fubar");
//
//        assertTrue(src_dobj.contains(0x00100010));
//        assertEquals( src_dobj.getString(s1), "Patient^Name");
//        assertEquals( src_dobj.getString(s2), "fubar");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_FROM_SINGULAR_TAGPATH_TO_PUBLIC_TAG));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(0x00100010), "fubar");
//        assertEquals( result_dobj.getString(0x00080020), "fubar");
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void testAssignFromPluralTagPathToPublicTag() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s1 = {0x00100010};
//        src_dobj.putString( s1, "Patient^Name");
//
//        assertTrue(src_dobj.contains(0x00100010));
//        assertEquals( src_dobj.getString(s1), "Patient^Name");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_FROM_PLURAL_TAGPATH_TO_PUBLIC_TAG));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//    }
//
//    @Test
//    public void testAssignFromFunctionToPublicTag() throws MizerException {
//        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();
//
//        int[] s1 = {0x00100010};
//        src_dobj.putString( s1, "Patient^Name");
//        int[] s2 = {0x00080020};
//        src_dobj.putString( s2, "all f*ed up.");
//
//        assertTrue(src_dobj.contains(0x00100010));
//        assertEquals( src_dobj.getString(s1), "Patient^Name");
//        assertEquals( src_dobj.getString(s2), "all f*ed up.");
//
//        final ScriptApplicator sa = new ScriptApplicator(bytes(ASSIGN_FROM_FUNCTION_TO_PUBLIC_TAG));
//        final DicomObjectI result_dobj = sa.apply(src_dobj);
//
//        assertEquals( result_dobj.getString(0x00100010), "situation normal all f*ed up.");
//        assertEquals( result_dobj.getString(0x00080020), "all f*ed up.");
//    }


    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

}
