/*
 * DicomEdit: TestFunctions
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Run tests of shiftDateTimeByIncrement function.
 *
 */
public class TestShiftDateTimeByIncrementFunction {

    /**
     * Test date shift in simple tags.
     *
     * @throws MizerException
     */
    @Test
    public void testSimpleTags() throws MizerException {

        String script =
                "(0008,002A) := shiftDateTimeByIncrement[ (0008,002A), \"600\"]\n" +
                        "(0018,9074) := shiftDateTimeByIncrement[ (0018,9074), \"-600\"]";

        SimpleDateFormat df = new SimpleDateFormat( "yyyyMMddHHmmss.S");

        Long increment = 600000l;
        Date dateTime = new Date();
        Date dateTimeShiftedUp = new Date( dateTime.getTime() + increment);
        Date dateTimeShiftedDown = new Date( dateTime.getTime() - increment);

        final DicomObjectI src_dobj = createSimpleTestObject( df.format( dateTime));

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( df.format( dateTime), src_dobj.getString(acquisitionDateTime));
        assertEquals( df.format( dateTime), src_dobj.getString(frameAcquisitionDateTime));

        final ScriptApplicator sa = new ScriptApplicator(bytes(script));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( df.format( dateTimeShiftedUp), result_dobj.getString(acquisitionDateTime));
        assertEquals( df.format( dateTimeShiftedDown), result_dobj.getString(frameAcquisitionDateTime));
    }

    @Test
    public void testSequenceTags() throws MizerException {

        String script =
                "shiftDateTimeSequenceByIncrement[ \"600\", \"(5200,9230)/(0020,9111)/(0018,9074)\"]\n" +
                        "shiftDateTimeSequenceByIncrement[ \"-600\", \"(5200,9230)/(0020,9111)/(0018,9151)\"]";

        SimpleDateFormat df = new SimpleDateFormat( "yyyyMMddHHmmss.S");

        Long increment = 600000l;
        Date dateTime = new Date();
        Date dateTimeShiftedUp = new Date( dateTime.getTime() + increment);
        Date dateTimeShiftedDown = new Date( dateTime.getTime() - increment);

        final DicomObjectI src_dobj = createSequenceTestObject( df.format( dateTime));

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( df.format( dateTime), src_dobj.getString(p00));
        assertEquals( df.format( dateTime), src_dobj.getString(p01));
        assertEquals( df.format( dateTime), src_dobj.getString(p10));
        assertEquals( df.format( dateTime), src_dobj.getString(p11));

        assertEquals( df.format( dateTime), src_dobj.getString(q00));
        assertEquals( df.format( dateTime), src_dobj.getString(q01));
        assertEquals( df.format( dateTime), src_dobj.getString(q10));
        assertEquals( df.format( dateTime), src_dobj.getString(q11));

        final ScriptApplicator sa = new ScriptApplicator(bytes(script));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertEquals( "PatientName", src_dobj.getString(patientName));

        assertEquals( df.format( dateTimeShiftedUp), result_dobj.getString(p00));
        assertEquals( df.format( dateTimeShiftedUp), result_dobj.getString(p01));
        assertEquals( df.format( dateTimeShiftedUp), result_dobj.getString(p10));
        assertEquals( df.format( dateTimeShiftedUp), result_dobj.getString(p11));

        assertEquals( df.format( dateTimeShiftedDown), result_dobj.getString(q00));
        assertEquals( df.format( dateTimeShiftedDown), result_dobj.getString(q01));
        assertEquals( df.format( dateTimeShiftedDown), result_dobj.getString(q10));
        assertEquals( df.format( dateTimeShiftedDown), result_dobj.getString(q11));
    }

    /**
     * Inspired by DE-40
     * Note precision is rounded to milliseconds.
     */
    @Test
    public void testFractionalTimes() {
        try {
            String script =
                    "(0008,0030) := \"20200101120000\"\n" +
                            "(0008,0030) := shiftDateTimeByIncrement[(0008,0030) , \"10\"]\n" +
                            "(0008,0031) := \"20200101120000.\"\n" +
                            "(0008,0031) := shiftDateTimeByIncrement[(0008,0031) , \"10\"]\n" +
                            "(0008,0032) := \"20200101120000.4\"\n" +
                            "(0008,0032) := shiftDateTimeByIncrement[(0008,0032) , \"10\"]\n" +
                            "(0008,0033) := \"20200101120000.050\"\n" +
                            "(0008,0033) := shiftDateTimeByIncrement[(0008,0033) , \"10\"]\n" +
                            "(0008,0034) := \"20200101120000.800000\"\n" +
                            "(0008,0034) := shiftDateTimeByIncrement[(0008,0034) , \"10\"]\n" +
                            "(0008,0035) := \"20200101120000.800444\"\n" +
                            "(0008,0035) := shiftDateTimeByIncrement[(0008,0035) , \"10\"]\n" +
                            "(0008,0036) := \"20200101120000.800666\"\n" +
                            "(0008,0036) := shiftDateTimeByIncrement[(0008,0036) , \"10\"]\n" ;

            final ScriptApplicator sa = new ScriptApplicator(bytes(script));
            DicomObjectI dobj = DicomObjectFactory.newInstance();
            dobj = sa.apply( dobj);

            assertEquals( "20200101120010", dobj.getString( 0x00080030));
            assertEquals( "20200101120010", dobj.getString( 0x00080031));
            assertEquals( "20200101120010.4", dobj.getString( 0x00080032));
            assertEquals( "20200101120010.050", dobj.getString( 0x00080033));
            assertEquals( "20200101120010.800000", dobj.getString( 0x00080034));
            assertEquals( "20200101120010.800444", dobj.getString( 0x00080035));
            assertEquals( "20200101120010.800666", dobj.getString( 0x00080036));
        }
        catch( MizerException e) {
            fail("Unexpected exception: " + e);
        }
    }


    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    private DicomObjectI createSimpleTestObject(String dateTimeString) {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        src_dobj.putString(patientName, "PatientName");

        src_dobj.putString(acquisitionDateTime, dateTimeString);
        src_dobj.putString(frameAcquisitionDateTime, dateTimeString);

        return src_dobj;
    }

    private DicomObjectI createSequenceTestObject(String dateTimeString) {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        src_dobj.putString(patientName, "PatientName");

        src_dobj.putString(p00, dateTimeString);
        src_dobj.putString(p01, dateTimeString);
        src_dobj.putString(p10, dateTimeString);
        src_dobj.putString(p11, dateTimeString);

        src_dobj.putString(q00, dateTimeString);
        src_dobj.putString(q01, dateTimeString);
        src_dobj.putString(q10, dateTimeString);
        src_dobj.putString(q11, dateTimeString);

        return src_dobj;
    }

    private static int[] patientName = {0x00100010};

    private static int[] acquisitionDateTime = {0x0008002A};
    private static int[] frameAcquisitionDateTime = {0x00189074};

    private static int[] p00 = {0x52009230, 0, 0x00209111, 0, 0x00189074};
    private static int[] p01 = {0x52009230, 0, 0x00209111, 1, 0x00189074};
    private static int[] p10 = {0x52009230, 1, 0x00209111, 0, 0x00189074};
    private static int[] p11 = {0x52009230, 1, 0x00209111, 1, 0x00189074};

    private static int[] q00 = {0x52009230, 0, 0x00209111, 0, 0x00189151};
    private static int[] q01 = {0x52009230, 0, 0x00209111, 1, 0x00189151};
    private static int[] q10 = {0x52009230, 1, 0x00209111, 0, 0x00189151};
    private static int[] q11 = {0x52009230, 1, 0x00209111, 1, 0x00189151};

}
