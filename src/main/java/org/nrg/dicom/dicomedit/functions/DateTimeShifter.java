package org.nrg.dicom.dicomedit.functions;

import org.dcm4che2.util.DateUtils;

import java.util.Calendar;
import java.util.Date;

public interface DateTimeShifter {

    default String shiftDateTime( int increment, String dateTimeString) {
        if( dateTimeString == null || dateTimeString.isEmpty()) {
            return dateTimeString;
        }
        else {
            String dateStringSeconds = truncateFractionalSeconds( dateTimeString);
            String dateStringFractionalSeconds = getFractionalSeconds( dateTimeString);

            final Date date = DateUtils.parseDT( dateStringSeconds, false);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.SECOND, increment);

            String shiftedDTString = DateUtils.formatDT(c.getTime());
            String shiftedDTSecondsString = truncateFractionalSeconds( shiftedDTString);
            StringBuffer sb = new StringBuffer( shiftedDTSecondsString);
            if( ! dateStringFractionalSeconds.isEmpty()) {
                sb.append(".").append( dateStringFractionalSeconds);
            }
            return sb.toString();
        }
    }

    default String truncateFractionalSeconds( String dateTimeString) {
        String dtString;
        if (dateTimeString == null || dateTimeString.isEmpty()) {
            dtString = "";
        } else if (dateTimeString.contains(".")) {
            dtString = dateTimeString.substring(0, dateTimeString.lastIndexOf('.'));
        } else {
            dtString = dateTimeString;
        }
        return dtString;
    }

    /**
     * String does not include the '.'.
     *
     * @param dateTimeString
     * @return
     */
    default String getFractionalSeconds( String dateTimeString) {
        String fracSecondsString;
        if( dateTimeString == null || dateTimeString.isEmpty()) {
            fracSecondsString = "";
        }
        else if( dateTimeString.contains(".")) {
            fracSecondsString = dateTimeString.substring( dateTimeString.lastIndexOf('.')+1);
        }
        else {
            fracSecondsString = "";
        }
        return fracSecondsString;
    }
}
