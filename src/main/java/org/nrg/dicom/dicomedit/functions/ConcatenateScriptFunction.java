/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.ConcatenateFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;


import com.google.common.base.Joiner;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;

import java.util.List;

/**
 * Created by drm on 4/20/16.
 */
public class ConcatenateScriptFunction extends AbstractScriptFunction {

    public ConcatenateScriptFunction() {
        super("concatenate", AbstractScriptFunction.DEFAULT_NAMESPACE, "Usage: ", "Description: ");
    }

    public Value apply(final List<Value> args, DicomObjectI dicomObject) {
        return new ConstantValue(Joiner.on("").join(args));
    }
}
