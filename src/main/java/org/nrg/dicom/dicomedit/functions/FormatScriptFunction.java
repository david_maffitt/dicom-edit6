/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.FormatFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;


import org.nrg.dicom.mizer.exceptions.ScriptEvaluationRuntimeException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;

import java.text.MessageFormat;
import java.util.List;

/**
 * Created by drm on 4/24/16.
 */
public class FormatScriptFunction extends AbstractScriptFunction {

    public FormatScriptFunction() {
        super("format", AbstractScriptFunction.DEFAULT_NAMESPACE, "Usage: ", "Description: ");
    }

    @Override
    public Value apply(List<Value> values, DicomObjectI dicomObject) {
        try {
            return values.size() > 0 ? new ConstantValue(MessageFormat.format(values.get(0).asString(), values.subList(1, values.size()).toArray())) : null;
        } catch( IllegalArgumentException e) {
            throw new ScriptEvaluationRuntimeException(getFormattedErrorMessage(values));
        }
    }
}
