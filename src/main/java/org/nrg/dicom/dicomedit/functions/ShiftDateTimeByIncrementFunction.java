/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.HashUIDFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;

import java.util.List;

public class ShiftDateTimeByIncrementFunction extends AbstractScriptFunction implements DateTimeShifter {

    public ShiftDateTimeByIncrementFunction() {
        super("shiftDateTimeByIncrement", AbstractScriptFunction.DEFAULT_NAMESPACE, "Usage: ", "Description: ");
    }

    @Override
    public Value apply(final List<Value> values, DicomObjectI dicomObject) throws ScriptEvaluationException {
        if (values.size() != 2) {
            throw new ScriptEvaluationException("usage: shiftDateTimeByIncrement[src-date increment-in-seconds ]");
        }

        String dateString = values.get(0).asString();
        final int increment = values.get(1).asInteger();

        String shiftedDate = shiftDateTime( increment, dateString);
        return new ConstantValue(shiftedDate);
    }

}
