/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.Function
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;

import com.google.common.base.Joiner;
import org.nrg.dicom.dicomedit.annotation.DicomEditFunction;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Properties;

/**
 * Base class for all script functions
 */
@DicomEditFunction(name = "base_function", namespace = "")
public abstract class AbstractScriptFunction {
    public static AbstractScriptFunction createInstance(Properties properties) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final String                                        fqn           = properties.getProperty("class");
        final Class<? extends AbstractScriptFunction>       functionClass = Class.forName(fqn).asSubclass(AbstractScriptFunction.class);
        final Constructor<? extends AbstractScriptFunction> constructor   = functionClass.getConstructor(Properties.class);
        return constructor.newInstance(properties);
    }

    public AbstractScriptFunction(String name, String nameSpace, String usage, String description) {
        this.name = name;
        this.nameSpace = nameSpace;
        this.usage = usage;
        this.description = description;
        this.fqn = (DEFAULT_NAMESPACE.equals(nameSpace)) ? name : nameSpace + "_" + name;
    }

    public AbstractScriptFunction(Properties properties) {
        name = properties.getProperty("name");
        if (name == null) {
            String msg = "Failed to create script function. Required property \'name\' is missing.";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
        nameSpace = properties.getProperty("namespace");
        if (nameSpace == null) {
            String msg = "Failed to create script function. Required property \'namespace\' is missing.";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
        usage = "";
        description = "";
        this.fqn = (DEFAULT_NAMESPACE.equals(nameSpace)) ? name : nameSpace + "_" + name;
    }

    abstract public Value apply(List<Value> args, DicomObjectI dicomObject) throws ScriptEvaluationException;

    public String getName() {
        return name;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public String getUsage() {
        return usage;
    }

    public String getDescription() {
        return description;
    }

    public String getFQN() {
        return fqn;
    }

    protected String getFormattedErrorMessage(final List<Value> values) {
        return "Error executing function " + getName() + "\nArguments: " + Joiner.on(", ").join(values) + "\n" + getUsage();
    }

    protected static final String DEFAULT_NAMESPACE = "";

    private String nameSpace;
    private String name;
    private String usage;
    private String description;
    private String fqn;

    private static final Logger logger = LoggerFactory.getLogger(AbstractScriptFunction.class);
}
