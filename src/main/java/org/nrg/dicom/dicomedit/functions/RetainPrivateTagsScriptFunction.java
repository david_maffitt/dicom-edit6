/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.UppercaseFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;

import org.nrg.dicom.dicomedit.TagPathFactory;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomElementI;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.objects.DicomObjectVisitor;
import org.nrg.dicom.mizer.tags.TagPath;
import org.nrg.dicom.mizer.tags.TagPrivate;
import org.nrg.dicom.mizer.values.AbstractMizerValue;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Retain private tags matching the provided white-listed tags, remove all other private tags.
 *
 */
public class RetainPrivateTagsScriptFunction extends AbstractScriptFunction {

    private static final Logger logger = LoggerFactory.getLogger(RetainPrivateTagsScriptFunction.class);

    public RetainPrivateTagsScriptFunction() {
        super("retainPrivateTags", AbstractScriptFunction.DEFAULT_NAMESPACE, "Usage: ", "Description: ");
    }

    @Override
    public Value apply(final List<Value> values, DicomObjectI dicomObject) throws ScriptEvaluationException {

        DicomObjectVisitor visitor = new DicomObjectVisitor() {
            @Override
            public void visitTag(TagPath tagPath, DicomElementI dicomElement, DicomObjectI dicomObject) {
                logger.debug( tagPath + " : " + dicomElement);
                if( tagPath.getLastTag() instanceof TagPrivate) {
                    for( Value v: values) {
                        try {
                            TagPath tp = TagPathFactory.createDE6Instance(v.asString());
                            if (tp.isMatch(tagPath)) {
                                return;
                            }
                        }
                        catch( MizerException e) {
                            String msg = "function arg tagPath: " + v.asString();
                            logger.error( msg);
//                            throw new ScriptEvaluationException( msg, e);
                        }
                    }
                    dicomObject.delete(tagPath);
//                    dicomObject.delete( tagPath.getTagsAsArray());
                }
            }
        };
        visitor.visit( dicomObject);

        return AbstractMizerValue.VOID;
    }
}
