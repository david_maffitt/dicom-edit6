/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.HashUIDFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ShiftDateByIncrementFunction extends AbstractScriptFunction {
    private DateFormat df;

    public ShiftDateByIncrementFunction() {
        super("shiftDateByIncrement", AbstractScriptFunction.DEFAULT_NAMESPACE, "Usage: ", "Description: ");
        this.df = new SimpleDateFormat("yyyyMMdd");
    }

    @Override
    public Value apply(final List<Value> values, DicomObjectI dicomObject) throws ScriptEvaluationException {
        if (values.size() != 2) {
            throw new ScriptEvaluationException("usage: shiftDateByIncrement[src-date increment-in-days ]");
        }

        String dateString = values.get(0).asString();
        final int increment = values.get(1).asInteger();

        String shiftedDate;
        if( dateString == null || dateString.isEmpty()) {
            shiftedDate = dateString;
        }
        else {
            try {
                final Date date = df.parse(dateString);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.DATE, increment);

                shiftedDate = df.format(c.getTime());

            } catch (ParseException e) {
                throw new ScriptEvaluationException(MessageFormat.format("Failed to parse date '{0}'. Expected yyyyMMdd", dateString), e);
            }
        }
        return new ConstantValue(shiftedDate);
    }
}
