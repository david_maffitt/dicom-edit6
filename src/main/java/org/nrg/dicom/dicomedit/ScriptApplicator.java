/*
 * DicomEdit: ScriptApplicator
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.dcm4che2.data.DicomObject;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.values.AbstractMizerValue;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The entry point into the library.
 *
 * The applicator parses the script on creation. Dicom objects are modified by calling the applicator's apply methods.
 */
public class ScriptApplicator implements ScriptApplicatorI {
    private static final Logger logger = LoggerFactory.getLogger(ScriptApplicator.class);
    private final DE6Script script;
    private final DicomEditParseTreeVisitor visitor;

    private static final List<String> versions = Arrays.asList("6.0", "6.1");

    /**
     * Construct the applicator from the specified anon-script stream.
     *
     * @param in {@link InputStream} to anon-script.
     *
     * @throws MizerException When an error occurs creating a DICOM object from the submitted file.
     */
    public ScriptApplicator(InputStream in) throws MizerException {
        try {
            script = new DE6Script(in);

            visitor = new DicomEditParseTreeVisitor();
        } catch(ScriptEvaluationException e) {
            throw new MizerException(e);
        }
    }

    /**
     * Apply this script to the specified DICOM object file.
     *
     * @param file {@link java.io.File}  The DICOM object file.
     *
     * @return {@link DicomObjectI} the modified DicomObject.
     *
     * @throws MizerException When an error occurs creating a DICOM object from the submitted file.
     */
    @Override
    public DicomObjectI apply(File file) throws MizerException {
        logger.info("Applying script to file: " + file);
        DicomObjectI dicomObject = DicomObjectFactory.newInstance(file);
        return apply(dicomObject);
    }

    /**
     * Apply this script to the specified DICOM object input stream.
     *
     * @param is {@link InputStream}  The DICOM object input stream.
     *
     * @return {@link DicomObjectI} the modified DicomObject.
     *
     * @throws MizerException When an error occurs creating a DICOM object from the submitted file.
     */
    @Override
    public DicomObjectI apply(InputStream is) throws MizerException {
        logger.debug("Applying script to stream.");
        DicomObjectI dicomObject = DicomObjectFactory.newInstance(is);
        return apply(dicomObject);
    }

    /**
     * Apply this script to the specified DICOM object.
     *
     * @param dicomObject The DICOM object.
     *
     * @return {@link DicomObjectI} the modified DicomObject.
     */
    @Override
    public DicomObjectI apply(DicomObjectI dicomObject) {

        logger.trace("Applying script to Dicom object: " + dicomObject);
        visitor.setDicomObject(dicomObject);
        visitor.visit( script.getParseTree());
        logger.trace("Edited Dicom object: " + dicomObject);
        return dicomObject;
    }

    @Override
    public Set<String> getVariableNames() {
        return script.getVariables().keySet();
    }

    @Override
    public Set<String> getExternalVariableNames() {
        return script.getExternalVariables().keySet();
    }

    /**
     * Return the {@link Variable} with the specified name.
     *
     * @param name The name of the variable.
     *
     * @return {@link Variable} the value of the named variable, null if it does not exist.
     */
    @Override
    public Variable getVariable(String name) {
        return visitor.getVariable(name);
    }

    /**
     * Return the {@link AbstractMizerValue} of the defined variable with the specified name.
     *
     * @param name The name of the variable.
     *
     * @return {@link AbstractMizerValue} the value of the named variable, null if the variable is not defined.
     */
    @Override
    public Value getValue(final String name) {
        final Variable variable = visitor.getVariable(name);
        return (variable == null) ? null : variable.getValue();
    }

    /**
     * Maximum tag value potentially required for or modified by this operation.
     *
     * Punt for now and assume everything up to Pixel Data is allowed.
     *
     * @return The value of the top tag.
     */
    @Override
    public long getTopTag() {
        return 0x7FE00010 - 1;
    }

    /**
     * Apply the script to the provided dcm4che2 dicom object.
     *
     * This is for backwards compatibility with anonymize package.
     *
     * @param matchFile   TODO:  What is this? Ignored for now.
     * @param dicomObject The DICOM object to be processed.
     *
     * @throws MizerException When an error occurs creating a DICOM object from the submitted file.
     */
    @Override
    public void apply(final File matchFile, final DicomObject dicomObject) throws MizerException {
        DicomObjectI de_dobj = DicomObjectFactory.newInstance( matchFile, dicomObject);
        apply(de_dobj);
    }

    /**
     * Return a map with variables.
     *
     * For compatibility with anonymize package which hardcodes use of dcm4che2.
     *
     * @return The variables for the script application.
     */
    @Override
    public Map<String, Variable> getVariables() {
        return visitor.getVariables();
    }

    /**
     * Set a variable.
     *
     * TODO: will want a "Variable" version too?
     *
     * @param name  The name of the variable to set.
     * @param value The value to set for the variable.
     */
    @Override
    public void setVariable(final String name, final String value) {
        visitor.setVariable(name, new ConstantValue(value));
    }

    /**
     * Return list of strings identifying DicomEdit script version supported.
     *
     * @return list of Dicom Edit script versions.
     */
    @Override
    public List<String> getSupportedVersions() {
        return versions;
    }

}
