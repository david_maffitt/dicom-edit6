package org.nrg.dicom.dicomedit.mizer;

import org.nrg.dicom.dicomedit.DE6Script;
import org.nrg.dicom.dicomedit.ScriptApplicator;
import org.nrg.dicom.mizer.exceptions.MizerContextException;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.VersionString;
import org.nrg.dicom.mizer.service.impl.AbstractMizer;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.dicom.mizer.tags.TagPath;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handle anonymization with DicomEdit v6 scripts.
 */
@Component
public class DE6Mizer extends AbstractMizer {

    // keep in sync with VersionManager.
    private static final List<VersionString> supportedVersions = Stream.of("6.0", "6.1", "6.2", "6.3").map(VersionString::new).collect(Collectors.toList());
    private static final Logger              logger            = LoggerFactory.getLogger(DE6Script.class);

    private final Map<MizerContextWithScript, ScriptApplicator> _contextMap;

    public DE6Mizer() {
        super(supportedVersions);
        _contextMap = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void aggregate(final MizerContext context, final Set<Variable> variables) {
        for (final Variable variable : variables) {
            context.setElement(variable.getName(), variable);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<TagPath> getScriptTags(final List<MizerContext> contexts) {
        return new HashSet<TagPath>() {{
            for (final MizerContext context : contexts) {
                addAll(getScriptTags(context));
            }
        }};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<TagPath> getScriptTags(final MizerContext context) {
        final Set<TagPath> tags = new HashSet<>();
        if (context instanceof MizerContextWithScript) {
            final MizerContextWithScript scriptContext = (MizerContextWithScript) context;
            try {
                final DE6Script script = new DE6Script(scriptContext.getScript());
                tags.addAll(script.getReferencedTags());
            } catch (MizerException e) {
                logger.warn("Error looking for referenced tags in script:\n{}", scriptContext.getScriptAsString(), e);
            }
        }
        return tags;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getReferencedVariables(final MizerContext context, final Set<Variable> variables) throws MizerContextException {
        if (context instanceof MizerContextWithScript) {
            final MizerContextWithScript scriptContext = (MizerContextWithScript) context;
            try {
                final DE6Script script = new DE6Script(scriptContext.getScript());
                variables.addAll(script.getVariables().values());
            } catch (MizerException e) {
                logger.warn("Error looking for referenced variables in script:\n{}", scriptContext.getScriptAsString(), e);
                if (e instanceof MizerContextException) {
                    throw (MizerContextException) e;
                }
                throw new MizerContextException(scriptContext, e);
            }
        }
    }

    @Override
    protected void anonymizeImpl(final DicomObjectI dicomObject, final MizerContextWithScript context) throws MizerException {
        ScriptApplicator applicator = getScriptApplicator( context);
        applicator.apply(dicomObject);
    }

    private ScriptApplicator getScriptApplicator( MizerContextWithScript context) throws MizerException {
        return (_contextMap.containsKey( context))? _contextMap.get( context): createScriptApplicator( context);
    }

    private ScriptApplicator createScriptApplicator( MizerContextWithScript context) throws MizerException {
        try (InputStream input = context.getScriptInputStream()) {
            final ScriptApplicator applicator = new ScriptApplicator(input);

            for (final String name : applicator.getExternalVariableNames()) {
                final String value = (String) context.getElement(name);
                if (value == null) {
                    throw new MizerException(MessageFormat.format("Missing value for script-required variable: {0}", name));
                } else {
                    applicator.setVariable(name, value);
                }
            }

            return applicator;
        }
        catch (IOException e) {
            throw new MizerException( e);
        }
    }

    @Override
    public void setContext( MizerContextWithScript context) throws MizerException {
        if( ! _contextMap.containsKey( context)) {
            _contextMap.put(context, createScriptApplicator(context));
        }
    }

    @Override
    public void removeContext( MizerContextWithScript context) {
        _contextMap.remove( context);
    }

    @Override
    protected String getMeaning() {
        return "XNAT DicomEdit 6 Script";
    }

    @Override
    protected String getSchemeDesignator() {
        return "XNAT";
    }

    @Override
    protected String getSchemeVersion() {
        return "1.0";
    }
}
